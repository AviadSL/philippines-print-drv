// __          __              _               _
// \ \        / /             (_)             | |
//  \ \  /\  / /_ _ _ __ _ __  _ _ __   __ _  | |
//   \ \/  \/ / _` | '__| '_ \| | '_ \ / _` | | |
//    \  /\  / (_| | |  | | | | | | | | (_| | |_|
//     \/  \/ \__,_|_|  |_| |_|_|_| |_|\__, | (_)
//                                      __/ |
//  _____           _           _     _|___/                    _
// |  __ \         (_)         | |   |  \/  |                  | |
// | |__) | __ ___  _  ___  ___| |_  | \  / | _____   _____  __| |
// |  ___/ '__/ _ \| |/ _ \/ __| __| | |\/| |/ _ \ \ / / _ \/ _` |
// | |   | | | (_) | |  __/ (__| |_  | |  | | (_) \ V /  __/ (_| |
// |_|   |_|  \___/| |\___|\___|\__| |_|  |_|\___/ \_/ \___|\__,_|
//                _/ |
//               |__/
//
// # MOVED this plugin's source code to be inside philippines-pos project!
// # MOVED this plugin's source code to be inside philippines-pos project!
// # MOVED this plugin's source code to be inside philippines-pos project!
package com.philippines.printer;
import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.InvalidParameterException;
import android_serialport_api.SerialPort;
import hdx.HdxUtil;
import java.util.TimeZone;
import android.provider.Settings;
import android.content.Context;
import android.app.AlarmManager;
import java.util.Calendar;
import android.content.Intent;
import android.app.PendingIntent;
public class PhilippinesPlugin extends CordovaPlugin{
  private AlarmManager alarmMgr;
  private PendingIntent alarmIntent;
  protected SerialPort mSerialPort = null;
  public SerialPort getPort() throws IOException {
    String path = HdxUtil.GetPrinterPort();
    int baudrate = 115200;
    mSerialPort = new SerialPort(path, baudrate, 0);
    return mSerialPort;
  }
  @Override public boolean execute( String action
                                  , JSONArray data
                                  , CallbackContext callbackContext
                                  ) throws JSONException {
    if(action.equals("print")){
      String name = data.getString(0);
      try {
        mSerialPort = getPort();
        HdxUtil.SwitchSerialFunction(HdxUtil.SERIAL_FUNCTION_PRINTER);
        HdxUtil.SetPrinterPower(1);// power on the printer
        OutputStream mOutputStream;
        mOutputStream = mSerialPort.getOutputStream();
        String printTest = name;
        mOutputStream.write(printTest.getBytes());
        mOutputStream.flush();
      }catch(IOException ex) {}
      callbackContext.success("Print");
      return true;
    }else if(action.equals("testversion")){
      try {
        String name = data.getString(0);
        callbackContext.success("[testversion][-013-] Hello, " + name);
      }catch(Exception ex) {
        callbackContext.success( "[com.philippines.printer plugin]"
                               + " testversion exception;" + ex.getMessage());
      }
      return true;
    }else if(action.equals("autotimeandtimezone")){
      try {
        if(Settings.Global.getInt( cordova.getActivity().getContentResolver()
                                  , Settings.Global.AUTO_TIME) != 0
          && Settings.Global.getInt(cordova.getActivity().getContentResolver()
                                  , Settings.Global.AUTO_TIME_ZONE) != 0) {
          callbackContext.success("On");
        }else{
          callbackContext.success("Off");
        }
      }catch(Exception ex) {
        callbackContext.success( "[com.philippines.printer plugin] "
                               + "autotimeandzone exception" + ex.getMessage());
      }
      return true;
    }else{
      return false;// meaning: no action found
    }
  }
}
