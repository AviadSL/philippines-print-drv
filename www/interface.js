// __          __              _               _
// \ \        / /             (_)             | |
//  \ \  /\  / /_ _ _ __ _ __  _ _ __   __ _  | |
//   \ \/  \/ / _` | '__| '_ \| | '_ \ / _` | | |
//    \  /\  / (_| | |  | | | | | | | | (_| | |_|
//     \/  \/ \__,_|_|  |_| |_|_|_| |_|\__, | (_)
//                                      __/ |
//  _____           _           _     _|___/                    _
// |  __ \         (_)         | |   |  \/  |                  | |
// | |__) | __ ___  _  ___  ___| |_  | \  / | _____   _____  __| |
// |  ___/ '__/ _ \| |/ _ \/ __| __| | |\/| |/ _ \ \ / / _ \/ _` |
// | |   | | | (_) | |  __/ (__| |_  | |  | | (_) \ V /  __/ (_| |
// |_|   |_|  \___/| |\___|\___|\__| |_|  |_|\___/ \_/ \___|\__,_|
//                _/ |
//               |__/
//
// # MOVED this plugin's source code to be inside philippines-pos project!
// # MOVED this plugin's source code to be inside philippines-pos project!
// # MOVED this plugin's source code to be inside philippines-pos project!
/*global cordova, module*/
module.exports = {
  greet: function (name, successCallback, errorCallback) {
    console.log('warning! Project moved, source code is inside philippines-pos');
    cordova.exec(successCallback, errorCallback, "PhilippinesPlugin", "print", [name]);
  },
  testversion: function (name, successCallback, errorCallback) {
    console.log('warning! Project moved, source code is inside philippines-pos');
    cordova.exec(successCallback, errorCallback, "PhilippinesPlugin", "testversion", [name]);
  },
  autotimeandtimezone: function (name, successCallback, errorCallback) {
    console.log('warning! Project moved, source code is inside philippines-pos');
    cordova.exec(successCallback, errorCallback, "PhilippinesPlugin", "autotimeandtimezone", []);
  }
};
